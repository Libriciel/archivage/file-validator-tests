#!/usr/bin/php
<?php
require '../vendor/autoload.php';

$dir = isset($argv[1]) ? $argv[1] : '*';
$files = glob(dirname(__dir__).'/samples/'.$dir.'/*');

foreach ($files as $filename) {
    if (strpos($filename, 'bad_') || strpos($filename, 'broken_')) {
        echo 'File already exists : '.$filename."\n";
        continue;
    }

    if (preg_match('/^(.*\/)([^\/]+)$/', $filename, $match) && !file_exists($match[1].'bad_'.$match[2])) {
        $badFile = fopen($match[1].'bad_'.$match[2], 'w');
        $brokenFile = fopen($match[1].'broken_'.$match[2], 'w');

        fwrite($badFile, File\Corruptor\FileCorruptor::lowLevelCorruption($filename));
        fwrite($brokenFile, File\Corruptor\FileCorruptor::highLevelCorruption($filename));

        fclose($badFile);
        fclose($brokenFile);

        echo 'Created : '.$match[1].'bad_'.$match[2]."\n";
        echo 'Created : '.$match[1].'broken_'.$match[2]."\n";
    }
}
